# Install nfs server and define the shares in
# /etc/exports.d/gitlab.exports
#

case node['platform_family']
when 'debian'
  package 'nfs-kernel-server'

  template "/etc/default/nfs-kernel-server" do
    variables node['gitlab-nfs-cluster']['nfs-server'].to_hash
    notifies :restart, 'service[nfs-kernel-server]'
  end

  template "/etc/default/nfs-common" do
    variables node['gitlab-nfs-cluster']['nfs-server'].to_hash
    notifies :restart, 'service[nfs-kernel-server]'
  end

  template "/etc/modprobe.d/lockd-options.conf" do
    variables node['gitlab-nfs-cluster']['nfs-server'].to_hash
    notifies :restart, 'service[nfs-kernel-server]'
  end

  file "/etc/modules-load.d/lockd.conf" do
      content "lockd\n"
      mode '0644'
  end

  service 'nfs-kernel-server' do
    supports [:restart, :reload]
  end
end

directory "/etc/exports.d" do
  owner 'root'
  group 'root'
  mode '0755'
end

file "/etc/exports.d/gitlab.exports" do
  content "/var/opt/gitlab  10.0.0.0/8(rw,sync,no_subtree_check,no_root_squash)\n"
  mode '0644'
  notifies :run, 'execute[/usr/sbin/exportfs -a]'
end

execute "/usr/sbin/exportfs -a" do
  action :nothing
end
