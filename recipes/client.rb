return if skip_unsupported_platform
package 'nfs-common'

#
# Loop over hash create destination
# and mount to destination
#
node['gitlab-nfs-cluster']['servers'].sort.each do |name,options|
  Chef::Log.debug("Trying to mount the #{name} share with these options: #{options.to_s}")

  directory options['local_path'] do
    recursive true
  end

  mount options['local_path'] do
    device "#{options['server']}:#{options['remote_path']}"
    fstype 'nfs'
    options options['nfs_options'] || node['gitlab-nfs-cluster']['client']['default_options']
    pass 0
    if options['enabled']
      action [:enable, :mount]
    else
      action [:disable, :umount]
    end
  end
end
