# Install nfs server and define the shares in
# /etc/exports.d/gitlab.exports
#
return if skip_unsupported_platform
package 'nfs-kernel-server'

service 'nfs-kernel-server' do
  supports [:restart, :reload]
end

# we have specific settings 
template "/etc/default/nfs-kernel-server" do
  notifies :restart, 'service[nfs-kernel-server]'
end


directory "/etc/exports.d" do
  owner 'root'
  group 'root'
  mode '0755'
end

# Generate actual exports file
template '/etc/exports.d/gitlab.exports' do
  source 'gitlab.exports.erb'
  mode '0644'
  notifies :run, 'execute[/usr/sbin/exportfs -a]'
end

#
# mount will fail if the directory does not exist,
# so we have to create it if we are on an empty machine
#
node['gitlab-nfs-cluster']['exports'].each do |_,export|
  directory export['path']
end

execute "/usr/sbin/exportfs -a" do
  action :nothing
end
