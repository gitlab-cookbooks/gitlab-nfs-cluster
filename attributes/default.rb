default['gitlab-nfs-cluster']['nfs-server']['rpcnfsdcount'] = 256 # Ubuntu default is 8, let's get more
default['gitlab-nfs-cluster']['nfs-server']['rpc_statd_port'] = 4000 # Static port for statd to listen on
default['gitlab-nfs-cluster']['nfs-server']['rpc_lockd_port'] = 4001 # Static port for lockd to listen on
default['gitlab-nfs-cluster']['nfs-server']['rpc_mountd_port'] = 4002 # Static port for mountd to listen on
default['gitlab-nfs-cluster']['client']['default_options'] = "defaults,soft,rsize=1048576,wsize=1048576,timeo=50,noatime,lookupcache=positive"
