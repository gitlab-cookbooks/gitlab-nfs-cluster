# gitlab-nfs-cluster-cookbook

This is the cookbook that runs on all workers and NFS servers. This includes NFS stuff.

## Supported Platforms

It is intended to run on the GitLab workers and NFS servers within the Azure Cloud Service.

## Attributes

### Server
The following attribute tree is used by the server to create it's export file points:
```json
"gitlab-nfs-cluster": {
	"exports":{
		"gitlab-default": {
			"path": "/var/opt/gitlab",
			"source": "10.0.0.0/8",
			"options": "rw,sync,no_subtree_check,no_root_squash"
		}
	}
}
```


* **path** - Local path which the nfs share will export 
* **source** - Ip address with CIDR
* **options** - String of nfs options to use for this share


### Client
The following attribute tree is used by the client to create it's mount points:
```json
"gitlab-nfs-cluster": {
	"servers":{
		"nfs-server01": {
			"local_path": "/path/to/local/destination/",
			"remote_path": "/path/to/remote/share",
			"server": "10.0.0.1",
			"enabled": true,
			"nfs_options": "rw,soft,intr,tcp,async,noatime,_netdev,nolock"
		}
	}
}
```

* **local_path** - Local path where the nfs share will be mounted to
* **remote_path** - Path on the remote server to the NFS share
* **server** - Ip address of the remote server
* **enabled** - Mount this mount point if true or unmount it if false (*Default:* true)
* **nfs_options** - mount options to pass on to the mount resource (*Default:* defaults,soft,rsize=1048576,wsize=1048576,timeo=10,noatime,lookupcache=positive)

## Usage

### gitlab-nfs-cluster::server

Include `gitlab-nfs-cluster::server` in your node's `run_list` with a share defined via the attributes above:

```json
{
  default_attributes: {
    "gitlab-nfs-cluster": {
      "exports":{
        "gitlab-default": {
          "path": "/var/opt/gitlab",
          "source": "10.0.0.0/8",
          "options": "rw,sync,no_subtree_check,no_root_squash"
        }
      }
    }
  },
  "run_list": [
    "recipe[gitlab-nfs-cluster::server]"
  ]
}
```
This will create the exports.d/ entry and enable the `nfs-server` daemon

### gitlab-nfs-cluster::client

Include `gitlab-nfs-cluster::client` in your node's `run_list` with a share defined via the attributes above:

```json
{
  default_attributes: {
    "gitlab-nfs-cluster": {
      "servers":{
        "nfs-server01": {
          "local_path": "/path/to/local/destination/",
          "remote_path": "/path/to/remote/share",
          "server": "10.0.0.1",
          "enabled": true,
          "nfs_options": "rw,soft,intr,tcp,async,noatime,_netdev,nolock"
        }
      }
    }
  },
  "run_list": [
    "recipe[gitlab-nfs-cluster::client]"
  ]
}
```
This will try to connect with the server to ensure that the mount will succeed. It then creates the folder for the share, and mounts the share **WITHOUT** creating an `/etc/fstab` entry. It then fills a template with the shares which can be called inorder to mount the shares at anytime the server is availible in the same way `mount -a` would if the mountpoints had been defined in the `/etc/fstab`.

## Testing
### Rspec
The server and client recipes have sane dafaults tests.

### Kitchen
The server and client have been configured to be able to test each other: 
* first `converge` the server so the client has someone to talk to
* only then can the client run and also establish the connection

**Caveat**: 
There is still a test which has been commented since the mount options `rsize` and `wsize` are not being honored in kitchen!


## License and Authors
