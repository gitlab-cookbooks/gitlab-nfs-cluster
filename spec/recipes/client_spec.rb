require_relative "../spec_helper"

describe "gitlab-nfs-cluster::client" do

  let(:chef_run) {
    ChefSpec::SoloRunner.new { |node|
      node.default['gitlab-nfs-cluster']['servers']['file01'] = { 
        local_path: '/var/opt/gitlab',
        remote_path: '/var/opt/gitlab/',
        server: '10.1.0.1',
        enabled: true
      }
		}.converge(described_recipe)

	}

	it "installs nfs-common" do
		expect(chef_run).to install_package('nfs-common')
	end

	  it "mounts an NFS share with defaults" do
	    expect(chef_run).to enable_mount('/var/opt/gitlab')
	                        .with(device: '10.1.0.1:/var/opt/gitlab/', options: 'defaults,soft,rsize=1048576,wsize=1048576,timeo=50,noatime,lookupcache=positive'.split(','))
	  end

end
