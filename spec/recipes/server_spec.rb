require_relative "../spec_helper"

describe "gitlab-nfs-cluster::server" do
  let(:chef_run) {
    ChefSpec::SoloRunner.new { |node|
      node.default['gitlab-nfs-cluster']['exports']['gitlab-default'] = { 
        path: '/var/opt/gitlab',
        source: '10.0.0.0/8',
        options: 'rw,sync,no_subtree_check,no_root_squash'
      }
    }.converge(described_recipe)
                                        
  }

  it "installs nfs-kernel-server" do
    expect(chef_run).to install_package('nfs-kernel-server')
  end

  it "creates a template with the export" do
    expect(chef_run).to create_template('/etc/exports.d/gitlab.exports')
    expect(chef_run).to render_file('/etc/exports.d/gitlab.exports').with_content('/var/opt/gitlab 10.0.0.0/8(rw,sync,no_subtree_check,no_root_squash)')
    template = chef_run.template('/etc/exports.d/gitlab.exports')
    expect(template).to notify('execute[/usr/sbin/exportfs -a]').to(:run).delayed
  end

  it "creates the target directory" do
    expect(chef_run).to create_directory('/var/opt/gitlab')
  end
end
