# 0.1.13

- add share01 for common items

# 0.1.12

- add LFS NFS server

# 0.1.11

- adding multiple NFS servers for sharding 

# 0.1.10

- adding back ssh serving

# 0.1.9

- removing git-data-alt server and thus from this cookbook

# 0.1.8

- HAProxy recipes are moved to gitlab-haproxy cookbook

# 0.1.7

- Add landing support for feedback.gitlab.com

# 0.1.6

- Add landing support for ce.gitlab.com and ee.gitlab.com

# 0.1.5

- fixed and error with redirecting jobs.gitlab.com

# 0.1.4

- Add jobs.gitlab.com landing redirection

# 0.1.3

- Set pass to 0 for nfs mounts

# 0.1.2

- Add the git-data-alt mountpoint

# 0.1.0

Initial release of gitlab-nfs-cluster
