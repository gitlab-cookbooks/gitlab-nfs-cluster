require "spec_helper"


describe "NFS kernel package configuration" do
  context package("nfs-common") do
    it { should be_installed }
  end
end

describe "NFS mounts setup correctly" do

  describe file("/var/opt/gitlab") do
    it { should be_directory }
  end

  describe file("/var/opt/gitlab") do
    it do
    should be_mounted.with(
        :device  => "10.1.0.1:/var/opt/gitlab",
        :type    => "nfs4",
        :options => {
#          :rsize    => 1048576,
#          :wsize    => 1048576,
#          :defaults => true,
          :soft     => true,
          :timeo    => 50,
          :noatime  => true,
          :lookupcache => 'pos'
        }
			)
		end
	end
end

#describe "Mount script configured correctly" do

#end
