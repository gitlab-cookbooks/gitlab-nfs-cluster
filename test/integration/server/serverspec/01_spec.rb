require "spec_helper"


describe "NFS server package configuration" do
  context package("nfs-kernel-server") do
    it { should be_installed }
  end

  context file("/etc/default/nfs-kernel-server") do
    it { should contain "RPCNFSDCOUNT=256" }
  end
end

describe "NFS Service State" do
  context command("service nfs-server status") do
    its(:exit_status) { should eq 0 }
  end

  describe file('/var/opt/gitlab') do
    it { should be_directory }
  end

  context command("exportfs") do
    its(:stdout) { should match %r{/var/opt/gitlab$\s+10\.0\.0\.0/8$}m}
    its(:exit_status) { should eq 0 }
  end

end
